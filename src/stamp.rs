use num_traits::{Zero, One};
use std::ops::AddAssign;
use rand::{Rand, random};

pub struct IncrementalStamper<S, C> {
    site:   S,
    clock:  C
}
impl<S, C> IncrementalStamper<S, C> where S: Copy, C: AddAssign + One + Copy {
    pub fn stamp(&mut self) -> (S, C) {
        self.clock += C::one();
        (self.site, self.clock)
    }
}
impl<S, C> IncrementalStamper<S, C> where S: Rand, C: Zero{
    pub fn init_random() -> IncrementalStamper<S, C> {
        IncrementalStamper {
            site: random(),
            clock: C::zero()
        }
    }
}
impl<S, C> IncrementalStamper<S, C> where C: Zero{
    pub fn init_with_site(site: S) -> IncrementalStamper<S, C> {
        IncrementalStamper {
            site: site,
            clock: C::zero()
        }
    }
}
