// (C) Sebastian Köln <sebk@rynx.org> 2016

//! Algorith from the following research paper:
//!
//! [Gérald Oster, Pascal Urso, Pascal Molli, Abdessamad Imine.
//! Real time group editors without Operational transformation.
//! [Research Report] RR-5580, INRIA. 2005, pp.24. <inria-00071240>](https://hal.inria.fr/inria-00071240)

extern crate itertools;
extern crate rand;
extern crate num_traits;

pub mod stamp;
pub use stamp::IncrementalStamper;

use std::clone::Clone;
use std::fmt::Debug;
use std::fmt::{Formatter, Error};
use std::collections::{HashMap, LinkedList};
use std::iter::Iterator;
use std::hash::Hash;

pub trait Key : Eq + Ord + Copy + Hash {}
impl<S, C> Key for (S, C) where
    S: Eq + Ord + Copy + Hash,
    C: Eq + Ord + Copy + Hash {}
    
#[derive(Eq, PartialEq, Ord, PartialOrd, Copy, Clone)]
pub enum Index<I> where I: Key {
    Start,
    Ident(I),
    End
}
impl<I> Debug for Index<I> where I: Key + Debug {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        match self {
            &Index::Start => write!(fmt, "Start"),
            &Index::Ident(ref id) => id.fmt(fmt),
            &Index::End => write!(fmt, "End")
        }
    }
}


#[derive(PartialEq, Copy, Clone, Debug)]
enum ListIndex {
    Start,
    Mid(usize),
    End
}

/// The symbol type of the string.
// W-charcter
// except the visible attribute was integrated with the value as an Option.
// Represents one character and can be almost anything.
#[derive(Clone)]
pub struct Character<S, I: Key> {
    id:         I,
    value:      Option<S>,   // Some -> visible, None -> invisible
    prev:       Index<I>,    // id_cp
    next:       Index<I>     // id_cn
}
impl<S, I> Debug for Character<S, I> where S: Debug, I: Key + Debug {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        match &self.value {
            &Some(ref v) => write!(fmt,
                "C({:?} < {:?} @ {:?} < {:?})", self.prev, v, self.id, self.next
            ),
            &None => write!(fmt,
                "C({:?} < Del @ {:?} < {:?})", self.prev, self.id, self.next
            )
        }
    }
}
impl<S, I: Key> Character<S, I> {
    /// Create a new Charater.
    // 
    /// Only use when deserialising a existing character.
    pub fn new(id: I, value: S, prev: Index<I>, next: Index<I>) -> Character<S, I> {
        Character {
            id:     id,
            value:  Some(value),
            prev:   prev,
            next:   next
        }
    }
}

/// Represents an Operation on the WString.
#[derive(Clone)]
pub enum Operation<S, I: Key> {
    /// insert a new Charater
    Ins(Character<S, I>),
    
    /// delete character with the given Key
    Del(I)
}
impl<S: Debug, P: Key + Debug> Debug for Operation<S, P> {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        match self {
            &Operation::Ins(ref c) => write!(fmt, "Ins({:?})", c),
            &Operation::Del(k) => write!(fmt, "Del({:?})", k)
        }
    }
}

pub struct WStringIter<'a, S: 'a, P: Key + 'a> {
    data:   &'a [Character<S, P>],
    pos:    usize,
    size:   usize
}
impl<'a, S, P: Key> Iterator for WStringIter<'a, S, P> {
    type Item = &'a S;
    fn next(&mut self) -> Option<&'a S> {
        for pos in self.pos .. self.data.len() {
            if let Some(ref v) = self.data[pos].value {
                self.pos = pos + 1;
                return Some(v);
            }
        }
        None
    }
    
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.size, Some(self.size))
    }
}

/// The actual String type
pub struct WString<S, P: Key> {
    data:       Vec<Character<S, P>>,
    buf:        HashMap<P, LinkedList<Operation<S, P>>>,
    size:       usize
}
impl<S, P> Debug for WString<S, P> where S: Debug, P: Key + Debug {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        try!(writeln!(fmt, "WString"));
        for (stamp, items) in &self.buf {
            try!(writeln!(fmt, "  queue for {:?}", stamp));
            for item in items.iter() {
                try!(writeln!(fmt, "    {:?}", item));
            }
        }
        for (i, item) in self.data.iter().enumerate() {
            try!(writeln!(fmt, "  data[{:4}]: {:?}", i, item));
        }
        Ok(())
    }
}

impl<S, P> WString<S, P> where S: Clone, P: Key {
    pub fn new() -> WString<S, P> {
        WString {
            data:       vec![],
            buf:        HashMap::new(),
            size:       0
        }
    }

    /// Length of visible data.
    pub fn len(&self) -> usize {
        self.size
    }
    
    /// Visible data
    pub fn contents(&self) -> Vec<S> {
        self.data.iter().filter_map(|c| c.value.clone()).collect()
    }
    
    /// Visible data as an iterator
    pub fn iter(&self) -> WStringIter<S, P> {
        WStringIter::<S, P> {
            data:   &self.data,
            pos:    0,
            size:   self.size
        }
    }
    
    fn pos_ident(&self, id: P, start: usize) -> Option<usize> {
        self.data.iter().skip(start).position(|c| c.id == id).map(|i| i + start)
    }
    
    // pos(S, c)
    fn pos(&self, idx: Index<P>, start: usize) -> Option<ListIndex> {
        match idx {
            Index::Ident(id) => 
                self.pos_ident(id, start).map(|i| ListIndex::Mid(i)),
            Index::Start => Some(ListIndex::Start),
            Index::End => Some(ListIndex::End)
        }
    }
    
    // insert(S, c, p)
    // insert Character c at position p
    fn insert(&mut self, c: Character<S, P>, p: usize) {
        self.data.insert(p, c);
        self.size += 1;
    }
    
    // integrate between prev and next
    fn integrate_ins(&mut self, c: Character<S, P>, mut prev: ListIndex, mut next: ListIndex) {
        use std::cmp::Ordering;
        use itertools::Itertools;
        use std::iter::once;
        
        loop {
            // i_0 is the first element to access
            let i_0 = match prev {
                ListIndex::Start => 0,
                ListIndex::Mid(n) => n + 1,
                ListIndex::End => panic!("got End as prev id")
            };
            
            // i_(n-1) is the last element to access
            // i_n is forbidden and may be 0
            let i_n = match next {
                ListIndex::Start => panic!("got Start as next id"),
                ListIndex::Mid(n) => n,
                ListIndex::End => self.data.len()
            };
            
            //println!("i_0={}, i_n={}", i_0, i_n);
            if i_0 == i_n {
                self.insert(c, i_n);
                return;
            }
            
            // characters that do not fullfill this filter are ignored
            let outside = |d: &[Character<S, P>], c: &Character<S, P>| {
                d.iter()
                .map(|c| Index::Ident(c.id))
                .all(|idx| idx != c.prev && idx != c.next)
            };
            
            let it = self.data[i_0 .. i_n].iter()
            .enumerate() // we have to track the indices!
            .filter(|&(_, c)| outside(&self.data[i_0 .. i_n], c)) // ignore any inner characters
            .map(|(id, c_i)| (ListIndex::Mid(i_0 + id), c_i.id.cmp(&c.id)));
            
            let it2 = once((prev, Ordering::Less))
            .chain(it).chain(once((next, Ordering::Greater)))
            .peekable()
            .batching(|mut it| match (it.next(), it.peek()) {
                (Some(a), Some(&b)) => Some((a, b)),
                _ => None
            })
            .peekable();
            
            let mut c_p = prev;
            let mut c_n = next;
            for ((p_idx, _), (n_idx, n_cmp)) in it2 {
                c_p = p_idx;
                c_n = n_idx;
                
                if n_cmp != Ordering::Less {
                    break;
                }
            }
            
            assert!(c_p != prev || c_n != next, "no progress!");
            prev = c_p;
            next = c_n;
        }
    }
    
    fn enqueue(&mut self, id: P, op: Operation<S, P>) {
        self.buf.entry(id).or_insert_with(LinkedList::new).push_front(op)
    }
    
    fn delete(&mut self, p: usize) {
        match self.data[p].value.take() {
            Some(_) => {
                self.size -= 1;
            },
            None => {
                println!("already deleted");
            }
        }
    }
    
    /// Applies Operation op.
    pub fn apply(&mut self, op: Operation<S, P>) {
        // remove reference when borrowck allows so
        match op {
            Operation::Ins(c) => {
                if let Some(prev) = self.pos(c.prev, 0) {
                    if let Some(next) = self.pos(c.next, 0) { // set start to prev
                        /*
                        println!("integrate {:?} between {:?} < {:?}", c, prev, next);
                        for (j, d) in self.data.iter().enumerate() {
                            println!("{:2} {:?}", j, d);
                        }
                        */
                        let id = c.id;
                        self.integrate_ins(c, prev, next);
                        /*
                        println!("now:");
                        for (j, d) in self.data.iter().enumerate() {
                            println!("{:2} {:?}", j, d);
                        }
                        */
                        // remove the entry. The id is now known.
                        match self.buf.remove(&id) {
                            Some(list) => {
                                for op in list.into_iter() {
                                    self.apply(op);
                                }
                            },
                            None => {}
                        }
                    } else { // next node missing
                        match c.next {
                            Index::Ident(next) => self.enqueue(next, Operation::Ins(c)),
                            _ => unreachable!()
                        }
                    }
                } else { // prev is missing
                    match c.prev {
                        Index::Ident(prev) => self.enqueue(prev, Operation::Ins(c)),
                        _ => unreachable!()
                    }
                }
            },
            Operation::Del(id) => {
                if let Some(i) = self.pos_ident(id, 0) {
                    self.data[i].value = None;
                } else {
                    self.enqueue(id, Operation::Del(id));
                }
            }
        }
    }
    
    /// Insert v at position n.
    /// This will panic if n > len()
    pub fn ins(&mut self, n: usize, v: S, stamp: P) -> Operation<S, P> {
        let (c, prev, next) = {
            let mut it = self.data.iter()
            .enumerate()
            .filter(|&(_, c)| c.value.is_some());
            
            let (prev_idx, prev) = match n {
                0 => (ListIndex::Start, Index::Start), 
                _ => {
                    let (idx, c) = it.by_ref().skip(n-1)
                    .next().expect("out of bounds");
                    (ListIndex::Mid(idx), Index::Ident(c.id))
                }
            };
            let (next_idx, next) = match it.next() {
                Some((idx, c)) => (ListIndex::Mid(idx), Index::Ident(c.id)),
                None           => (ListIndex::End, Index::End)
            };
            
            let c = Character {
                id:         stamp,
                value:      Some(v),
                prev:       prev,
                next:       next
            };
            
            (c, prev_idx, next_idx)
        };
        // WRONG !! that would be integrate between S[idx] and S[idx+1]
        // we need nthVisible(n-1) and nthVisible(n)
        // self.insert(&c, idx+1);
        
        // double check
        // pos returns indices offset by +1
        //assert_eq!(self.pos(c.prev, 0), Some(prev));
        //assert_eq!(self.pos(c.next, 0), Some(next));
        // println!("send integrate {:?} between {:?} < {:?}", c, prev, next);
        self.integrate_ins(c.clone(), prev, next);
        
        Operation::Ins(c)
    }
    
    /// delete charater at position n
    /// panics if n >= len()
    pub fn del(&mut self, n: usize) -> Operation<S, P> {
        let (p, id) = self.data.iter()
        .enumerate()
        .filter(|&(_, c)| c.value.is_some())
        .map(|(i, c)| (i, c.id))
        .skip(n)
        .next().expect("out of bounds");
        
        self.delete(p);
        
        Operation::Del(id)
    }
}


