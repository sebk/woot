extern crate woot;
use woot::WString;

#[test]
fn test_tp2() {
    let mut s = WString::new();
    s.ins(0, 'a');
    println!("s: {:?}", s.contents());
    s.ins(1, 'b');
    println!("s: {:?}", s.contents());
    s.ins(2, 'c');
    println!("s: {:?}", s.contents());
    s.ins(3, 'd');
    println!("s: {:?}", s.contents());
    
    let mut s1 = s.clone();
    let mut s2 = s.clone();
    let mut s3 = s.clone();
    
    let o1 = s1.ins(3, 'x');
    let o2 = s2.del(1);
    let o3 = s3.ins(2, 'y');
    
    let result: Vec<char> = "aycxd".chars().collect();
    
    // test each possiblity
    {
        let mut s1 = s1.clone();
        println!("s1: {:?}", s1.contents());
        s1.apply(o2.clone());
        println!("s1 ∘ o2: {:?}", s1.contents());
        s1.apply(o3.clone());
        println!("s1 ∘ o2 ∘ o3: {:?}", s1.contents());
        assert_eq!(s1.contents(), result);
        assert_eq!(s1.iter().cloned().collect::<Vec<char>>(), result);
    }
    {
        let mut s1 = s1.clone();
        s1.apply(o3.clone());
        s1.apply(o2.clone());
        assert_eq!(s1.contents(), result);
    }
    {
        let mut s2 = s2.clone();
        s2.apply(o1.clone());
        s2.apply(o3.clone());
        assert_eq!(s2.contents(), result);
    }
    {
        let mut s2 = s2.clone();
        s2.apply(o3.clone());
        s2.apply(o1.clone());
        assert_eq!(s2.contents(), result);
    }
    {
        let mut s3 = s3.clone();
        s3.apply(o1.clone());
        s3.apply(o2.clone());
        assert_eq!(s3.contents(), result);
    }
    {
        let mut s3 = s3.clone();
        s3.apply(o2.clone());
        s3.apply(o1.clone());
        assert_eq!(s3.contents(), result);
    }
}
